#include <stdio.h>
#include <stdlib.h>

int main()
{
   
    FILE *e;
    FILE *small;
    FILE *medium;
    FILE *large;
    
  
    e = fopen("homelistings.csv", "r");
    small = fopen("small.txt", "w");
    medium = fopen("medium.txt", "w");
    large = fopen("large.txt", "w");
    
    if (!e || !small || !medium || !large)
    {
        printf("Can't open file for reading\n");
        exit(1);
    }
    
    int zip, ID, area, price, beds, baths;
    char address [40];
    
    while (fscanf(e, "%d,%d,%[^,],%d, %d ,%d,%d  ", &zip, &ID, address, &price, &beds, &baths, &area) != EOF)
    {
      if(area < 1000)
        {
            
            fprintf(small, "%s : %d\n", address, area);
        }
        if(1000<=area<=2000)
        {
            fprintf(medium, "%s : %d\n", address, area);
        }
        if(2000<area)
        {
            fprintf(large, "%s : %d\n", address, area);
        }
        

    }
    
    fclose(e);
    fclose(small);
    fclose(medium);
    fclose(large);
}